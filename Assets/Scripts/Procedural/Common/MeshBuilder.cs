using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBuilder {
    /** 
     * Possible TO-DO:
     * -Tangents 
     * -Vertex colours
     * -Reserving space for very large meshes
     * -Ability to alter existing instances of the Mesh class.
     */

    //// Initialise the arrays that will contain our mesh data.
    //// A quad contains 4 vertice and 2 triangles:

    // The vertex positions of the mesh.
    // Always requiered:
    private List<Vector3> m_Vertices = new List<Vector3>();
    public List<Vector3> Vertices { get { return m_Vertices; } }

    // The vertex normals of the mesh.
    // Needed for lighning:
    private List<Vector3> m_Normals = new List<Vector3>();
    public List<Vector3> Normals { get { return m_Normals; } }

    // The UV coordinates of the mesh.
    // Needed to apply textures:
    private List<Vector2> m_UVs = new List<Vector2>();
    public List<Vector2> UVs { get { return m_UVs; } }

    
    //// Indices for the triangles
    private List<int> m_Indices = new List<int>();



    //// Adds a triangle to the mesh.
    // Each index (index0, index1, etc) is related to the index in the array of vertices.
    public void AddTriangle (int index0, int index1, int index2)
    {
        m_Indices.Add(index0);
        m_Indices.Add(index1);
        m_Indices.Add(index2);
    }

    //// Initialises an instance of the Unity Mesh class, based on the stored values.
    // And returns the mesh completed.
    public Mesh CreateMesh ()
    {
        Mesh mesh = new Mesh();
        
        // Add our vertex and triangle values to the new mesh:
        mesh.vertices = m_Vertices.ToArray();
        mesh.triangles = m_Indices.ToArray();

        // Normals are optional. Only use them if we have the correct amount:
        if (m_Normals.Count == m_Vertices.Count)
            mesh.normals = m_Normals.ToArray();

        // UVs are optional. Only use them if we have the correct amount:
        if (m_UVs.Count == m_Vertices.Count)
            mesh.uv = m_UVs.ToArray();

        // Have the mesh recalculate its bounding box (or its volume). 
        // This is required for proper rendering:
        mesh.RecalculateBounds();

        return mesh;
    }
}
